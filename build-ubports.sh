#!/bin/bash

set -Eeou pipefail

apt-get download libgles1-mesa-dev:${ARCH}

dpkg-deb -R libgles1-mesa-dev_*.deb libgles1-mesa-dev
mv -n libgles1-mesa-dev/usr/include/GLES .

wget -N http://ftp.hetzner.de/ubuntu/packages/pool/main/m/mesa/libgles1-mesa_11.2.0-1ubuntu2_${ARCH}.deb

dpkg-deb -R libgles1-mesa_*.deb libgles1-mesa
cp libgles1-mesa/usr/lib/${ARCH_TRIPLET}/mesa-egl/libGLESv1_CM.so.1.1.0 /tmp/libGLESv1_CM.so

export C_INCLUDE_PATH="$PWD"
export CPLUS_INCLUDE_PATH="$PWD"

cd "${ROOT}"
make -j4 ENABLE_TITLE=false
